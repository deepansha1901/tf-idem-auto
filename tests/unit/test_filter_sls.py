import json


def test_filter_sls(hub):
    expected_filtered_sls_data_path = (
        f"{hub.test.tf_idem_auto.current_path}/files/filtered_sls_data.json"
    )
    expected_tf_resource_map_path = (
        f"{hub.test.tf_idem_auto.current_path}/files/tf_resource_map.json"
    )

    with open(expected_filtered_sls_data_path) as file:
        data = file.read()
        expected_filtered_sls_data = json.loads(data)
    with open(expected_tf_resource_map_path) as file:
        data = file.read()
        expected_tf_resource_map = json.loads(data)

    (
        tf_resource_type_name_and_resource_map,
        filtered_sls_data,
    ) = hub.tf_idem_auto.sls_file_filter.filter_sls()
    assert tf_resource_type_name_and_resource_map and filtered_sls_data
    assert expected_tf_resource_map == tf_resource_type_name_and_resource_map
    assert expected_filtered_sls_data == filtered_sls_data
