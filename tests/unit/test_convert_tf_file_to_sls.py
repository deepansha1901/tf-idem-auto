import json
import os
import shutil


def test_convert_tf_file_to_sls(hub):
    filtered_sls_data_path = (
        f"{hub.test.tf_idem_auto.current_path}/files/filtered_sls_data.json"
    )
    tf_resource_map_path = (
        f"{hub.test.tf_idem_auto.current_path}/files/tf_resource_map.json"
    )
    with open(filtered_sls_data_path) as file:
        data = file.read()
        filtered_sls_data = json.loads(data)
    with open(tf_resource_map_path) as file:
        data = file.read()
        tf_resource_map = json.loads(data)
    tf_file = "eks-worker-iam-role.tf"
    root = f"{hub.test.tf_idem_auto.current_path}/resources/input/iam/eks-worker-iam-role.tf"
    tfvars_data = {
        "region": "eu-west-3",
        "profile": "test-dev",
        "owner": "org1",
        "cogs": "OPEX",
        "default_domain": "potato-dev.com",
        "singleAz": True,
        "pam_cluster_access": True,
        "singleAzIndex": 0,
        "admin_users": ["user1", "user2", "user3", "user4", "user5"],
        "cluster_admin": ["user1"],
        "cluster_edit": ["user1"],
        "cluster_read": ["user1"],
    }
    output_sls_path = f"{hub.test.tf_idem_auto.current_path}/output"
    tf_file_content = hub.tf_idem_auto.utils.parse_tf_data(root)

    (
        idem_resource_id_map,
        idem_resource_id_tf_resource_map,
        variables,
    ) = hub.tf_idem_auto.tf_sls.convert_tf_file_to_sls(
        root, tf_file, filtered_sls_data, tfvars_data, tf_resource_map, output_sls_path
    )
    assert not variables
    assert idem_resource_id_map and idem_resource_id_tf_resource_map
    sls_path = f"{output_sls_path}/sls/eks-worker-iam-role.sls"
    sls_resource_names = set()
    sls_file_content = hub.tf_idem_auto.utils.parse_sls_data(sls_path)
    for key in sls_file_content.keys():
        sls_resource_names.add(key.split(".")[1])

    tf_resource_names = set()
    resources = tf_file_content.get("resource")
    for tf_resource in resources:
        tf_resource_type = list(tf_resource.keys())[0]
        tf_resource_names.add(list(tf_resource[tf_resource_type].keys())[0])
    assert sls_resource_names == tf_resource_names

    for filename in os.listdir(output_sls_path):
        file_path = os.path.join(output_sls_path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(f"Failed to delete {file_path}. Reason: {e}")
