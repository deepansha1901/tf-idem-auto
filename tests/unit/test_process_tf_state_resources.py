import json


def test_process_tf_state_resources(hub):
    list_of_modules = {"module.iam", "module.cluster"}
    tf_state_resources_path = (
        f"{hub.test.tf_idem_auto.current_path}/files/tf_state_resources.json"
    )
    with open(tf_state_resources_path) as file:
        data = file.read()
        tf_state_resources = json.loads(data)

    check_idem_resource_types = {
        "aws.ec2.elastic_ip",
        "aws.cloudwatch.log_group",
        "aws.rds.db_subnet_group",
    }

    (
        processed_tf_state_resources,
        idem_resource_types,
    ) = hub.tf_idem_auto.tf_state_data_processor.process_tf_state_resources(
        tf_state_resources, list_of_modules
    )

    assert processed_tf_state_resources and idem_resource_types
    assert idem_resource_types == check_idem_resource_types
    assert tf_state_resources[0] in processed_tf_state_resources
    assert tf_state_resources[1] in processed_tf_state_resources
    assert tf_state_resources[2] in processed_tf_state_resources
    assert tf_state_resources[3] not in processed_tf_state_resources
