
aws_iam_role.cluster-node:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.cluster-node")}}
  - name: {{ params.get("clusterName") }}-temp-xyz-cluster-node
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster-node
  - id: AROAX2FJ77DC6POVG7T5L
  - path: /
  - max_session_duration: 3600
  - tags: {{ params.get("local_tags") }}
  - assume_role_policy_document: {"Version": "2012-10-17", "Statement": [{"Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}, "Action": "sts:AssumeRole"}]}

aws_iam_role_policy.Amazon_EBS_CSI_Driver:
  aws.iam.role_policy.present:
  - resource_id: {{ params.get("aws_iam_role_policy.Amazon_EBS_CSI_Driver")}}
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - name: Amazon_EBS_CSI_Driver
  - policy_document: {"Version": "2012-10-17", "Statement": [{"Effect": "Allow", "Action":
      ["ec2:AttachVolume", "ec2:CreateSnapshot", "ec2:CreateTags", "ec2:CreateVolume",
      "ec2:DeleteSnapshot", "ec2:DeleteTags", "ec2:DeleteVolume", "ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeSnapshots", "ec2:DescribeTags", "ec2:DescribeVolumes",
      "ec2:DescribeVolumesModifications", "ec2:DetachVolume", "ec2:ModifyVolume"],
      "Resource": "*"}]}

# ToDo: The attribute 'policy_document' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
aws_iam_role_policy.credstash_access_policy:
  aws.iam.role_policy.present:
  - resource_id: {{ params.get("aws_iam_role_policy.credstash_access_policy")}}
  - role_name: ${aws.iam.role:aws_iam_role.cluster-node:resource_id}
  - name: credstash_xyz_{{ params.get("clusterName") }}_access_policy
  - policy_document: {"Version": "2012-10-17", "Statement": [{"Action": ["kms:GenerateDataKey",
      "kms:Decrypt"], "Effect": "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"},
      {"Action": ["dynamodb:PutItem", "dynamodb:GetItem", "dynamodb:Query", "dynamodb:Scan"],
      "Effect": "Allow", "Resource": "arn:aws:dynamodb:{{ params.get(\"region\") }}:${data.aws_caller_identity.current.account_id}:table/xyz-{{
      params.get(\"clusterName\") }}-credential-store"}]}
