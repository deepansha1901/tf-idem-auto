#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#  * VPC Flow logs
#  * Transit Gateway

resource "aws_vpc" "cluster" {
  count = var.create_vpc ? 1 : 0
  cidr_block           = "${var.VpcSuperNet}0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = merge(
    local.tags,
    {
      "Name"                                     = "${var.clusterName}-temp-xyz-cluster-node"
      "kubernetes.io/cluster/${var.clusterName}" = "shared"
    },
  )
}

data "aws_vpc" "vpc"{
  count = var.create_vpc ? 0 : 1
  # most_recent = true
  tags = {
    Name   = var.VpcName
  }
}

data "aws_subnets" "pvt_subnets" {
  count = var.create_subnets ? 0 : 3
  tags = {
    Name  = var.pvt_subnet_name
  }
}

data "aws_subnets" "public_subnets" {
  count = var.create_subnets ? 0 : 3
  tags = {
    Name  = var.public_subnet_name
  }
}

//private subnets for worker nodes
resource "aws_subnet" "cluster" {
  count = var.create_subnets ? 3 : 0

  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = var.create_vpc ? "${var.VpcSuperNet}${count.index * 64}.0/18" : var.cluster_pvt_subnet_cidr[count.index]
  vpc_id            = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id

  tags = merge(
    local.tags,
    {
      "Name"                                     = "${var.clusterName}-temp-xyz-node-private"
      "kubernetes.io/cluster/${var.clusterName}" = "shared"
      "kubernetes.io/role/internal-elb"          = "1"
    },
  )

  lifecycle {
    ignore_changes = [cidr_block]
  }
}


// public subnets for control plane
resource "aws_subnet" "xyz_public_subnet" {
  # count = var.create_subnets ? 3 : 0
  count = 3
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = var.create_vpc ? "${var.VpcSuperNet}${192 + count.index * 16}.0/20" : var.cluster_public_subnet_cidr[count.index]
  vpc_id            = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id

  tags = merge(
    local.tags,
    {
      "Name"                                     = "${var.clusterName}-temp-xyz-control-public"
      "kubernetes.io/cluster/${var.clusterName}" = "shared"
      "kubernetes.io/role/elb"                   = "1"
    },
  )

  lifecycle {
    ignore_changes = [cidr_block]
  }
}

resource "aws_internet_gateway" "cluster" {
  count = var.create_vpc ? 1 : 0
  vpc_id = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id
  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-temp-xyz"
    },
  )
}

// elastic ip for nat gateway
resource "aws_eip" "nat_eip" {
  count = 3
  vpc   = true
  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-natgw-eip-${count.index}"
    },
  )
}

// nat gateway
resource "aws_nat_gateway" "nat_gateway" {
  count         = 3
  allocation_id = aws_eip.nat_eip[count.index].id
  subnet_id     = aws_subnet.xyz_public_subnet[count.index].id
  depends_on    = [aws_internet_gateway.cluster]
  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-natgw-${count.index}"
    },
  )
}


resource "aws_route_table" "cluster" {
  count  = 3
  vpc_id = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway[count.index].id
  }

  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-xyz-private-${count.index}"
    },
  )

  lifecycle {
    ignore_changes = [route]
  }
}


//route tables for public subnets
resource "aws_route_table" "xyz_public" {
  count  = var.create_vpc ? 3 : 0
  vpc_id = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cluster[0].id
  }

  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-xyz-public-${count.index}"
    },
  )

  lifecycle {
    ignore_changes = [route]
  }
}


//private subnet-route table association
resource "aws_route_table_association" "cluster" {
  count = 3

  subnet_id      = var.create_subnets ? aws_subnet.cluster[count.index].id : data.aws_subnets.pvt_subnets[count.index].id
  route_table_id = aws_route_table.cluster[count.index].id
}

//public subnet-route table association
resource "aws_route_table_association" "xyz_public" {
  count  = var.create_vpc ? 3 : 0

  subnet_id      = aws_subnet.xyz_public_subnet[count.index].id
  route_table_id = aws_route_table.xyz_public[count.index].id
}

# vpc flow logs
resource "aws_flow_log" "redlock_flow_log" {
  log_destination = aws_cloudwatch_log_group.redlock_flow_log_group.arn
  iam_role_arn    = aws_iam_role.redlock_flow_role.arn
  vpc_id          = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id
  traffic_type    = "ALL"
}

resource "aws_cloudwatch_log_group" "redlock_flow_log_group" {
  name = "xyz-${var.clusterName}_redlock_flow_log_group"
  tags = local.tags
}

resource "aws_iam_role" "redlock_flow_role" {
  name = "xyz-${var.clusterName}_redlock_flow_role"
  tags = local.tags

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "redlock_flow_policy" {
  name = "xyz-${var.clusterName}_redlock_flow_policy"
  role = aws_iam_role.redlock_flow_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_vpc_dhcp_options" "vpc_options" {
  domain_name         = "us-west-2.compute.internal"
  domain_name_servers = ["AmazonProvidedDNS"]
  tags = merge(
    local.tags,
    {
      "Name" = "xyz-${var.clusterName}"
    },
  )
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id
  dhcp_options_id = aws_vpc_dhcp_options.vpc_options.id
}

data "aws_ec2_transit_gateway" "region-tgw" {
  count = var.transit-gw > 0 ? 1 : 0
  filter {
    name   = "tag:Name"
    values = ["${var.profile}-tgw-${var.region}"]
  }
}

resource "aws_ec2_transit_gateway_vpc_attachment" "region-tgw-cluster" {
  count              = var.transit-gw > 0 ? 1 : 0
  transit_gateway_id = data.aws_ec2_transit_gateway.region-tgw[0].id
  vpc_id             = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id
  subnet_ids         = var.create_subnets ? aws_subnet.cluster.*.id : data.aws_subnets.pvt_subnets.*.id
  tags = {
    Name = "${var.clusterName}-vpc-${var.region} <--> ${data.aws_ec2_transit_gateway.region-tgw[0].description}"
  }
}


## RDS Subnet Group
resource "aws_db_subnet_group" "db-subnet-group" {
  name        = "db-subnet-group-${var.clusterName}"
  description = "For Aurora rds"
  subnet_ids  = var.create_subnets ? aws_subnet.cluster.*.id : data.aws_subnets.pvt_subnets.*.id

  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-db-subnet-group"
    },
  )
}


#
# Elasticache Subnet Group Resources
#
resource "aws_elasticache_subnet_group" "default" {
  name        = "elasticache-subnet-group-${var.clusterName}"
  description = "For elastcache redis cluster"
  subnet_ids  = var.create_subnets ? aws_subnet.cluster.*.id : data.aws_subnets.pvt_subnets.*.id
}
