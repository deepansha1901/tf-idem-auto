import pathlib
import unittest.mock as mock

import dict_tools
import pytest


@pytest.fixture()
def hub(hub, test_config):
    hub.pop.sub.add(dyne_name="tf_idem_auto")
    hub.test = dict_tools.data.NamespaceDict(
        tf_idem_auto=dict(
            current_path=pathlib.Path(__file__).parent.resolve(),
            unit_test=True,
            persist_output=False,
        )
    )
    with mock.patch("sys.argv", ["state", "thing"]):
        hub.pop.config.load(["tf_idem_auto"], cli="tf_idem_auto", parse_cli=False)
    yield hub


def pytest_addoption(parser):
    parser.addoption(
        "--test_config", action="store", default="", help="test_config value"
    )


@pytest.fixture
def test_config(request):
    yield request.config.getoption("--test_config")


@pytest.fixture
def change_test_dir(monkeypatch):
    current_path = pathlib.Path(__file__).parent.resolve()
    monkeypatch.chdir(current_path / "resources" / "input")
